import re
import os
import random
import __main__

HERE = __main__.__file__

JABBERWOCKY_TEXT = '''
'Twas brillig, and the slithy toves
      Did gyre and gimble in the wabe:
All mimsy were the borogoves,
      And the mome raths outgrabe.

"Beware the Jabberwock, my son!
      The jaws that bite, the claws that catch!
Beware the Jubjub bird, and shun
      The frumious Bandersnatch!"

He took his vorpal sword in hand;
      Long time the manxome foe he sought--
So rested he by the Tumtum tree
      And stood awhile in thought.

And, as in uffish thought he stood,
      The Jabberwock, with eyes of flame,
Came whiffling through the tulgey wood,
      And burbled as it came!

One, two! One, two! And through and through
      The vorpal blade went snicker-snack!
He left it dead, and with its head
      He went galumphing back.

"And hast thou slain the Jabberwock?
      Come to my arms, my beamish boy!
O frabjous day! Callooh! Callay!"
      He chortled in his joy.

'Twas brillig, and the slithy toves
      Did gyre and gimble in the wabe:
All mimsy were the borogoves,
      And the mome raths outgrabe.
'''

def makedirs(path):
    try:
        os.makedirs(path)
    except OSError:
        pass

def bloviate(where, N, seed=None):
    # split Jabberwocky into unique lowercase words
    splitter = re.compile('[^A-Za-z]+')
    basewords = splitter.split(JABBERWOCKY_TEXT)
    basewords = list(set(word.lower() for word in basewords if word != ''))
    
    r = random.Random(seed)
    
    makedirs(where)
    pairs = [(r.choice(basewords),r.choice(basewords)) for k in xrange(N)]
    pairs.sort()    
    files = [os.path.join(where, pair[0], '%03d_%s.txt' % (k, pair[1])) for k,pair in enumerate(pairs)]
    for filename in files:
        print os.path.relpath(filename, HERE)
        makedirs(os.path.dirname(filename))
        nwords = r.randint(500,1000)
        sentence = []
        if r.random() < 0.9:
            words = basewords[:]
            words.append('awesome')
        else:
            words = basewords
        with open(filename, 'w') as f:
            for k in xrange(nwords):
                q = r.random()
                if q < 0.85:
                    # add a word
                    sentence.append(r.choice(words))
                elif q < 0.93:
                    # add a comma
                    if sentence and not sentence[-1].endswith(','):
                        sentence[-1] += ','
                elif sentence:
                    # print the sentence
                    sentence[0] = sentence[0].capitalize()
                    f.write(' '.join(sentence)+r.choice('...?!')+'\n')
                    sentence = []
                else:
                    # print a blank line
                    f.write('\n')
    
if __name__ == '__main__':
    thisdir = os.path.dirname(HERE)
    thisdir = '.' if thisdir == '' else thisdir
    bloviate(where=os.path.join(thisdir,'../output'), N=500, seed=1)
